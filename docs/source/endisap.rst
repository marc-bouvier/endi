Passer endi en mode SAP (Services à la personne)
================================================

Une instance peut être dédiée aux services à la personne (CAE SAP).

.. warning:: L'ensemble de l'instance enDi fonctionne alors en mode services à
   la personne : toutes les enseignes, toutes les factures… etc.


Dans le fichier .ini de l'application rajouter :

.. code-block:: console

   endi.includes =
       endi.plugins.sap

    [celery]
    imports =
        endi.plugins.sap.celery_jobs


Dans le fichier .ini de endi_celery rajouter :

.. code-block:: console


    pyramid.includes =
        endi.plugins.sap.panels

    [celery]
    imports =
        endi.plugins.sap.celery_jobs


cf aussi :doc:`decoupage`.
