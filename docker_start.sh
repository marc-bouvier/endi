#!/bin/bash -e
# ATTENTION : les fichiers docker ne sont actuellement maintenus par personne…

# Variables d'environnement et leur valeur par défaut quand elles n'existent pas.
export APP_DB_USER=${APP_DB_USER:endi}
export APP_DB_NAME=${APP_DB_NAME:endi}
export APP_DB_PASS=${APP_DB_PASS:endi}
export APP_DB_HOST=${APP_DB_HOST:db}

cat << EOF | mysql -u root -ppassword --host $APP_DB_HOST
      SET GLOBAL max_connect_errors=10000;

      DROP USER IF EXISTS $APP_DB_USER;
      DROP DATABASE  IF EXISTS $APP_DB_USER;
      CREATE DATABASE $APP_DB_NAME;
      CREATE USER '$APP_DB_USER' IDENTIFIED BY '$APP_DB_PASS';
      GRANT ALL PRIVILEGES ON $APP_DB_NAME.* TO '$APP_DB_USER';
      FLUSH PRIVILEGES;
EOF

. /endi/bin/activate
endi-admin development.ini syncdb
endi-admin development.ini useradd --user=endi --pwd=endi --firstname=Admin --lastname=Endi --group=admin --email=admin.endi@example.org
pserve development.ini
