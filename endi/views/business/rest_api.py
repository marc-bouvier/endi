from endi.models.company import Company
from endi.models.project import Project
from endi.models.project.business import Business
from endi.models.task import Task
from endi.views import (
    RestListMixinClass,
    BaseRestView,
)
from endi.forms.project.business import APIBusinessListSchema
from endi.views.business.routes import (
    BUSINESS_COLLECTION_API,
    BUSINESS_COMPANY_CUSTOMERS_COLLECTION_API,
)


class BusinessRestView(RestListMixinClass, BaseRestView):
    """
    Businesses REST view, scoped to company

       GET : return list of businesses (company should be provided as context)
    """

    list_schema = APIBusinessListSchema()

    def query(self):
        company = self.request.context
        assert isinstance(company, Company)

        q = Business.query()
        q = q.join(Business.project)
        q = q.filter(Project.company_id == company.id)
        q = q.filter(Business.closed == False)
        return q

    def filter_search(self, query, appstruct):
        search = appstruct["search"]
        if search:
            query = query.filter(
                Business.name.like("%" + search + "%"),
            )
        return query

    def filter_project_id(self, query, appstruct):
        project_id = appstruct.get("project_id")
        if project_id:
            query = query.filter(Business.project_id == project_id)
        return query

    def filter_customer_id(self, query, appstruct):
        customer_id = appstruct.get("customer_id")
        if customer_id:
            query = query.join(Business.tasks)
            query = query.filter(Task.customer_id == customer_id)
        return query


def includeme(config):
    config.add_rest_service(
        factory=BusinessRestView,
        route_name=BUSINESS_COLLECTION_API,
        collection_route_name=BUSINESS_COMPANY_CUSTOMERS_COLLECTION_API,
        view_rights="view.business",
        edit_rights="edit.business",
        add_rights="add.business",
        delete_rights="delete.business",
        collection_view_rights="list.business",
    )
