from endi.views import BaseRestView
from endi.views.status.rest_api import StatusLogEntryRestView
from endi.views.status.utils import get_visibility_options


class CompanyRestView(BaseRestView):
    """Read-only / item-only at the moment"""

    def form_config(self):
        return {"options": {"visibilities": get_visibility_options(self.request)}}


def includeme(config):
    config.add_rest_service(
        factory=CompanyRestView,
        route_name="/api/v1/companies/{id}",
        view_rights="view.company",
    )

    config.add_view(
        CompanyRestView,
        attr="form_config",
        route_name="/api/v1/companies/{id}",
        renderer="json",
        request_param="form_config",
        permission="view.company",
    )

    config.add_rest_service(
        StatusLogEntryRestView,
        "/api/v1/companies/{eid}/statuslogentries/{id}",
        collection_route_name="/api/v1/companies/{id}/statuslogentries",
        collection_view_rights="view.company",
        add_rights="view.company",
        view_rights="view.statuslogentry",
        edit_rights="edit.statuslogentry",
        delete_rights="delete.statuslogentry",
    )
