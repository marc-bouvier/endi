from endi.forms.third_party.customer import get_add_edit_customer_schema
from endi.forms.third_party.customer import get_list_schema
from endi.models.third_party import Customer
from endi.views import (
    BaseRestView,
    RestListMixinClass,
)
from endi.views.status.rest_api import StatusLogEntryRestView
from endi.views.status.utils import get_visibility_options
from endi.views.third_party.customer.base import SCHEMA_FACTORIES
from endi.views.third_party.customer.lists import CustomersListTools


class CustomerRestView(RestListMixinClass, CustomersListTools, BaseRestView):
    """
    Customer rest view

    collection : context Root

        GET : return list of customers (company_id should be provided)
    """

    list_schema = get_list_schema()

    def get_schema(self, submitted):
        if "formid" in submitted:
            schema = SCHEMA_FACTORIES[submitted["formid"]]()
        else:
            excludes = ("company_id",)
            schema = get_add_edit_customer_schema(excludes=excludes)
        return schema

    def query(self):
        return Customer.query().filter_by(company_id=self.context.id)

    def post_format(self, entry, edit, attributes):
        """
        Associate a newly created element to the parent company
        """
        if not edit:
            entry.company = self.context
        return entry

    def form_config(self):
        return {"options": {"visibilities": get_visibility_options(self.request)}}


def includeme(config):
    config.add_rest_service(
        factory=CustomerRestView,
        route_name="/api/v1/customers/{id}",
        collection_route_name="/api/v1/companies/{id}/customers",
        view_rights="view_customer",
        edit_rights="edit_customer",
        add_rights="add_customer",
        delete_rights="delete_customer",
        collection_view_rights="list_customers",
    )

    config.add_view(
        CustomerRestView,
        attr="form_config",
        route_name="/api/v1/customers/{id}",
        renderer="json",
        request_param="form_config",
        permission="edit_customer",
    )

    config.add_rest_service(
        StatusLogEntryRestView,
        "/api/v1/customers/{eid}/statuslogentries/{id}",
        collection_route_name="/api/v1/customers/{id}/statuslogentries",
        collection_view_rights="view_customer",
        add_rights="view_customer",
        view_rights="view.statuslogentry",
        edit_rights="edit.statuslogentry",
        delete_rights="delete.statuslogentry",
    )
