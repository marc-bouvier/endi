<%inherit file="${context['main_template'].uri}" />

<%block name='actionmenucontent'>
<div class='layout flex main_actions'>
    <a class='btn' href='/export/export_log'>
            ${api.icon('info-circle')}
            Historique des exports comptables
    </a>
</div>
</%block>

<%block name='content'>
<div>
	% if help_message is not None:
		<div class="alert alert-info">
            <span class="icon">${api.icon('info-circle')}</span>
			${help_message|n}
		</div>
	% endif

	<div>
		<ul class="nav nav-tabs" role="tablist">
			<% current = request.params.get('__formid__', list(forms.keys())[0]) %>
			% for form_name, form_datas in forms.items():
				<li role="presentation" class="${'active' if form_name==current else ''}">
					<a href="#${form_name}-container" aria-controls="${form_name}-container" role="tab" data-toggle="tab" tabindex='-1'>
						<%
						tab_title = form_datas['title']
						tab_title = tab_title.replace("Exporter les factures fournisseurs ", "")
						tab_title = tab_title.replace("Exporter les paiements fournisseurs ", "")
						tab_title = tab_title.replace("Exporter les factures ", "")
						tab_title = tab_title.replace("Exporter les encaissements ", "")
						tab_title = tab_title.replace("Exporter les paiements ", "")
						tab_title = tab_title.replace("Exporter les ", "")
						tab_title = tab_title.replace("Exporter des ", "")
						%>
						${tab_title.capitalize()}
					</a>
				</li>
			% endfor
		</ul>
	</div>

	<div class='tab-content'>
		% for form_name, form_datas in forms.items():
			<div role="tabpanel" id="${form_name}-container" class="tab-pane fade ${'in active' if form_name==current else ''}">
				% if form_name == current and check_messages is not None:
					<div class="alert alert-info">
                        <span class="icon">${api.icon('info-circle')}</span>
                        ${check_messages['title']}
					</div>
					% if check_messages['errors']:
						<div class="alert alert-danger">
							<p class='text-danger'>
								<span class="icon"><svg><use href="${request.static_url('endi:static/icons/endi.svg')}#danger"></use></svg></span>
							% for message in check_messages['errors']:
								<b>*</b> ${message|n}<br />
							% endfor
							</p>
						</div>
					% endif
				% endif
				${form_datas['form']|n}
			</div>
		% endfor
	</div>

    <div>
        <%
            export_type = request.path.split('/')[-1]
        %>
        ## FIXME generate table header and cells thanks to headers
        ## as defined in export/sage.py instead of hardcoding them ?
        ## This refactoring should be done when displaying recorded export #2089
        % if preview_items is not None:
            <div class="alert alert-info">
                <span class="icon">${api.icon('info-circle')}</span>
			        Ceci est une prévisualisation, il n'est pas garanti que
                    l'export final soit exactement le même (d'autres écritures
                    pourraient être passées entre cette prévisualisation et
                    l'export définitif).
		    </div>

            <div><br />${len(preview_items)} Résultat(s)</div>

            <div class='table_container'>
                % if export_type == "invoices":
                    <table class="top_align_table hover_table">
                        <thead>
                            <tr>
                                <th scope="col" class="col_text">Numéro de pièce</th>
                                <th scope="col" class="col_text">Code Journal</th>
                                <th scope="col" class="col_date">Date de pièce</th>
                                <th scope="col" class="col_text">Compte général</th>
                                <th scope="col" class="col_text">Numéro de facture</th>
                                <th scope="col" class="col_text">Compte tiers</th>
                                <th scope="col" class="col_text">Code taxe</th>
                                <th scope="col" class="col_text">Libellé</th>
                                <th scope="col" class="col_date">Date d'échéance</th>
                                <th scope="col" class="col_number">Débit</th>
                                <th scope="col" class="col_number">Crédit</th>
                                <th scope="col" class="col_text">Type de ligne</th>
                                <th scope="col" class="col_text" title="Numéro analytique">Numéro ana<span class="screen-reader-text">lytique</span>.</th>
                            </tr>
                        </thead>
                        <tbody>
                            % for item in preview_items:
                                <tr class='tableelement'>
                                    <td class="col_text">${item['num_facture']}</td>
                                    <td class="col_text">${item['code_journal']}</td>
                                    <td class="col_date">${item['date']}</td>
                                    <td class="col_text">${item['compte_cg']}</td>
                                    <td class="col_text">${item['num_facture']}</td>
                                    <td class="col_text">
                                        % if 'compte_tiers' in item:
                                            ${item['compte_tiers']}
                                        % endif
                                    </td>
                                    <td class="col_text">
                                        % if 'code_tva' in item:
                                            ${item['code_tva']}
                                        % endif
                                    </td>
                                    <td class="col_text">${item['libelle']}</td>
                                    <td class="col_text">
                                        % if 'echeance' in item:
                                            ${item['echeance']}
                                        % endif
                                    </td>

                                    <td class="col_number">
                                        % if 'debit' in item:
                                            ${api.format_float((item['debit'] or 0)/100000, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_number">
                                        % if 'credit' in item:
                                            ${api.format_float((item['credit'] or 0)/100000, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_text">${item['type_']}</td>
                                    <td class="col_text">
                                        % if 'num_analytique' in item:
                                            ${item['num_analytique']}
                                        % endif
                                    </td>
                                </tr>
                            % endfor
                        </tbody>
                    </table>
                % elif export_type == "payments" or export_type == "expense_payments":
                    <%
                        divider = 0
                        if export_type == "payments":
                            divider = 100000
                        else:
                            divider = 100
                    %>
                    <table class="top_align_table hover_table">
                        <thead>
                            <tr>
                                <th scope="col" class="col_text">Référence</th>
                                <th scope="col" class="col_text">Code Journal</th>
                                <th scope="col" class="col_text">Date de pièce</th>
                                <th scope="col" class="col_text">N° compte général</th>
                                <th scope="col" class="col_text">Mode de règlement</th>
                                <th scope="col" class="col_text">Compte tiers</th>
                                <th scope="col" class="col_text">Code taxe</th>
                                <th scope="col" class="col_text">Libellé</th>
                                <th scope="col" class="col_text">Débit</th>
                                <th scope="col" class="col_text">Crédit</th>
                                <th scope="col" class="col_text">Type de ligne</th>
                                <th scope="col" class="col_text" title="Numéro analytique">Numéro ana<span class="screen-reader-text">lytique</span>.</th>
                            </tr>
                        </thead>
                        <tbody>
                            % for item in preview_items:
                                <tr class='tableelement'>
                                    <td class="col_text">${item['reference']}</td>
                                    <td class="col_text">${item['code_journal']}</td>
                                    <td class="col_date">${item['date']}</td>
                                    <td class="col_text">${item['compte_cg']}</td>
                                    <td class="col_text">${item['mode']}</td>
                                    <td class="col_text">
                                        % if 'compte_tiers' in item:
                                            ${item['compte_tiers']}
                                        % endif
                                    </td>
                                    <td class="col_text">
                                        % if 'code_tva' in item:
                                            ${item['code_tva']}
                                        % endif
                                    </td>
                                    <td class="col_text">${item['libelle']}</td>
                                    <td class="col_number">
                                        % if 'debit' in item:
                                            ${api.format_float((item['debit'] or 0)/divider, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_number">
                                        % if 'credit' in item:
                                            ${api.format_float((item['credit'] or 0)/divider, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_text">${item['type_']}</td>
                                    <td class="col_text">
                                        % if 'num_analytique' in item:
                                            ${item['num_analytique']}
                                        % endif
                                    </td>
                                </tr>
                            % endfor
                        </tbody>
                    </table>
                % elif export_type == "supplier_invoices":
                <table class="top_align_table hover_table">
                        <thead>
                            <tr>
                                <th scope="col" class="col_text">Numéro de pièce</th>
                                <th scope="col" class="col_text">Code Journal</th>
                                <th scope="col" class="col_date">Date de pièce</th>
                                <th scope="col" class="col_text">N° compte général</th>
                                <th scope="col" class="col_text">Numéro de facture</th>
                                <th scope="col" class="col_text">Compte tiers</th>
                                <th scope="col" class="col_text">Code taxe</th>
                                <th scope="col" class="col_text">Libellé</th>
                                <th scope="col" class="col_number">Débit</th>
                                <th scope="col" class="col_number">Crédit</th>
                                <th scope="col" class="col_text">Type de ligne</th>
                                <th scope="col" class="col_text" title="Numéro analytique">Numéro ana<span class="screen-reader-text">lytique</span>.</th>
                            </tr>
                        </thead>
                        <tbody>
                            % for item in preview_items:
                                <tr class='tableelement'>
                                    <td class="col_text">${item['num_endi']}</td>
                                    <td class="col_text">${item['code_journal']}</td>
                                    <td class="col_date">${item['date']}</td>
                                    <td class="col_text">${item['compte_cg']}</td>
                                    <td class="col_text">
                                        % if 'num_feuille' in item:
                                            ${item['num_feuille']}
                                        % endif
                                    </td>
                                    <td class="col_text">
                                        % if 'compte_tiers' in item:
                                            ${item['compte_tiers']}
                                        % endif
                                    </td>
                                    <td class="col_text">
                                        % if 'code_tva' in item:
                                            ${item['code_tva']}
                                        % endif
                                    </td>
                                    <td class="col_text">${item['libelle']}</td>
                                    <td class="col_number">
                                        % if 'debit' in item:
                                            ${api.format_float((item['debit'] or 0)/100, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_number">
                                        % if 'credit' in item:
                                            ${api.format_float((item['credit'] or 0)/100, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_text">${item['type_']}</td>
                                    <td class="col_text">
                                        % if 'num_analytique' in item:
                                            ${item['num_analytique']}
                                        % endif
                                    </td>
                                </tr>
                            % endfor
                        </tbody>
                    </table>
                % elif export_type == "supplier_payments":
                    <table class="top_align_table hover_table">
                        <thead>
                            <tr>
                                <th scope="col" class="col_text">Numéro de pièce</th>
                                <th scope="col" class="col_text">Code Journal</th>
                                <th scope="col" class="col_date">Date de pièce</th>
                                <th scope="col" class="col_text">N° compte général</th>
                                <th scope="col" class="col_text">Mode de règlement</th>
                                <th scope="col" class="col_text">Compte tiers</th>
                                <th scope="col" class="col_text">Code taxe</th>
                                <th scope="col" class="col_text">Libellé</th>
                                <th scope="col" class="col_number">Débit</th>
                                <th scope="col" class="col_number">Crédit</th>
                                <th scope="col" class="col_text">Type de ligne</th>
                                <th scope="col" class="col_text" title="Compte analytique">Compte ana<span class="screen-reader-text">lytique</span>.</th>
                                <th scope="col" class="col_text">Référence</th>
                            </tr>
                        </thead>
                        <tbody>
                            % for item in preview_items:
                                <tr class='tableelement'>
                                    <td class="col_text">${item['num_endi']}</td>
                                    <td class="col_text">${item['code_journal']}</td>
                                    <td class="col_date">${item['date']}</td>
                                    <td class="col_text">${item['compte_cg']}</td>
                                    <td class="col_text">${item['mode']}</td>
                                    <td class="col_text">
                                        % if 'compte_tiers' in item:
                                            ${item['compte_tiers']}
                                        % endif
                                    </td>
                                    <td class="col_text">
                                        % if 'code_tva' in item:
                                            ${item['code_tva']}
                                        % endif
                                    </td>
                                    <td class="col_text">${item['libelle']}</td>
                                    <td class="col_number">
                                        % if 'debit' in item:
                                            ${api.format_float((item['debit'] or 0)/100, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_number">
                                        % if 'credit' in item:
                                            ${api.format_float((item['credit'] or 0)/100, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_text">${item['type_']}</td>
                                    <td class="col_text">
                                        % if 'num_analytique' in item:
                                            ${item['num_analytique']}
                                        % endif
                                    </td>
                                    <td class="col_text">${item['reference']}</td>
                                </tr>
                            % endfor
                        </tbody>
                    </table>
                % elif export_type == "expenses":
                    <table class="top_align_table hover_table">
                        <thead>
                            <tr>
                                <th scope="col" class="col_text">Numéro de
                                pièce</th>
                                <th scope="col" class="col_date">Code journal</th>
                                <th scope="col" class="col_text">Date de pièce</th>
                                <th scope="col" class="col_text">N° compte
                                général</th>
                                <th scope="col" class="col_text">Numéro de note
                                de dépenses</th>
                                <th scope="col" class="col_text">Numéro de
                                compte tiers</th>
                                <th scope="col" class="col_text">Code taxe</th>
                                <th scope="col" class="col_number">Libellé</th>
                                <th scope="col" class="col_number">Débit</th>
                                <th scope="col" class="col_number">Crédit</th>
                                <th scope="col" class="col_text">Type de
                                ligne</th>
                                <th scope="col" class="col_text" title="Numéro analytique">Numéro ana<span class="screen-reader-text">lytique</span>.</th>
                                <th scope="col" class="col_text">Référence</th>
                            </tr>
                        </thead>
                        <tbody>
                            % for item in preview_items:
                                <tr class='tableelement'>
                                    <td class="col_text">${item['num_endi']}</td>
                                    <td class="col_text">${item['code_journal']}</td>
                                    <td class="col_date">${item['date']}</td>
                                    <td class="col_text">${item['compte_cg']}</td>
                                    <td class="col_text">${item['num_feuille']}</td>
                                    <td class="col_text">
                                        % if 'compte_tiers' in item:
                                            ${item['compte_tiers']}
                                        % endif
                                    </td>
                                    <td class="col_text">
                                        % if 'code_tva' in item:
                                            ${item['code_tva']}
                                        % endif
                                    </td>
                                    <td class="col_text">${item['libelle']}</td>
                                    <td class="col_number">
                                        % if 'debit' in item:
                                            ${api.format_float((item['debit'] or 0)/100, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_number">
                                        % if 'credit' in item:
                                            ${api.format_float((item['credit'] or 0)/100, precision=2)|n}
                                            €
                                        % endif
                                    </td>
                                    <td class="col_text">${item['type_']}</td>
                                    <td class="col_text">
                                        % if 'num_analytique' in item:
                                            ${item['num_analytique']}
                                        % endif
                                    </td>
                                    <td class="col_text">${item['num_endi']}</td>
                                </tr>
                            % endfor
                        </tbody>
                    </table>
                %endif
            </div>
        % endif
    </div>
</div>
</%block>

