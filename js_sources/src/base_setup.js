/*
 * Setup the main ui elements
 */
import { attachTools } from "./tools.js";
import 'bootstrap/js/transition.js';
import 'bootstrap/js/dropdown.js';
import 'bootstrap/js/collapse.js';
import 'select2/dist/js/select2.js';
import 'select2/dist/js/i18n/fr';

$(function(){
    attachTools();
    var company_menu = $('#company-select-menu');
    if (!_.isNull(company_menu)){
        company_menu.select2({language: 'fr'});
        company_menu.on('change', function(){
            window.location = this.value;
        });
    }
    $('a[data-toggle=dropdown]').dropdown();
    $('a[data-toggle=collapse]').collapse();
});
