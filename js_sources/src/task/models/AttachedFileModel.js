import BaseModel from '../../base/models/BaseModel'

const AttachedFileModel = BaseModel.extend({
    // Define the props that should be set on your model
    props: ['name', 'id', 'description'],
    url() {
        return '/api/v1/files/' + this.get('id');
    },
})
export default AttachedFileModel;