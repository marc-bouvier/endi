import ValidationLimitToolbarAppClass from "common/components/ValidationLimitToolbarAppClass";
import Radio from 'backbone.radio';
import SmallResumeView from "../views/resume/SmallResumeView";
import {
    hideBtnLoader,
    showBtnLoader
} from "tools";


const ToolbarAppClass = ValidationLimitToolbarAppClass.extend({
    callSaveAll(event) {
        const btn = $(event.currentTarget);
        showBtnLoader(btn);
        Radio.channel('facade').request("save:all").then(
            () => {
                setTimeout(() => hideBtnLoader(btn), 200);
            }
        )
    },
    getSaveButton() {
        return {
            widget: 'button',
            option: {
                onclick: (event) => this.callSaveAll(event),
                title: "Sauvegarder",
                css: "btn icon only",
                icon: "save"
            }
        }
    },
    getMoreActions(actions) {
        const result = actions['more'].slice();
        result.unshift(this.getSaveButton())
        console.log("More");
        console.log(result);
        return result
    },
    getResumeView(actions) {
        const facade = Radio.channel('facade');
        const model = facade.request('get:model', 'total');
        return new SmallResumeView({
            model: model
        });
    }
})
const ToolbarApp = new ToolbarAppClass();
export default ToolbarApp;