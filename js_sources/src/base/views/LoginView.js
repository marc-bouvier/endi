import Mn from 'backbone.marionette';
import ModalBehavior from '../behaviors/ModalBehavior.js';
import Radio from 'backbone.radio';

var template = require('./templates/LoginView.mustache');

const LoginView = Mn.View.extend({
    id: 'login-modal',
    className: 'modal-overlay',
    behaviors: [ModalBehavior],
    template: template,
    url: '/api/login/v1',
    ui:{
        'form': 'form',
        'login': 'input[name=login]',
        'password': 'input[type=password]',
        'remember_me': 'input[name=remember_me]',
        'errors': '.errors',
    },
    events: {
        "submit @ui.form": "onSubmit",
    },
    onSubmit(event){
        event.preventDefault();
        this.getUI('errors').hide();
        var datas = {
            login: this.getUI('login').val(),
            password: this.getUI('password').val()
        };
        if (this.getUI('remember_me').is(':checked')){
            datas['remember_me'] = true;
        }
        var channel = Radio.channel('auth');
        channel.trigger('login', datas, this.success.bind(this), this.error.bind(this));
    },
    onModalBeforeClose(){
        console.log("Redirecting");
        window.location.replace('#');
    },
    success(){
        this.triggerMethod('close');
    },
    error(){
        this.getUI('errors').html("Erreur d'authentification");
        this.getUI('errors').show();
    }
});
export default LoginView;
